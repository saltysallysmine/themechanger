-- local color_conf = {
--   theme = "catppuccin-latte",
-- }
-- 
-- return color_conf

-- Catppuccin
-- vim.cmd.colorscheme "catppuccin-latte"

-- Gruvbox
vim.cmd.colorscheme "gruvbox"
vim.opt.background = "light"
